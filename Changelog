2024-12-20 cage

	* Changelog,
	* cl-colors2.asd,
	* colors.lisp,
	* introduction.org,
	* introduction.txt,
	* package-template.lisp,
	* package.lisp,
	* test.lisp:

	- increased version number;
	- added a fallback parsing algorithm for colors string representation;
	- updated docs.
	- updated the changelog.

2024-06-22 cage

	* Changelog,
	* cl-colors2.asd,
	* colors.lisp:

	- instructed 'as-rgb' will convert the first argument to string when
	is a symbol, then will try to parse the string.
	- increased version number;
	- updated changelog.

2024-03-25 cage


	Merge pull request 'parse-hsv-hsl-strings' (#2) from
	parse-hsv-hsl-strings into master

2024-03-24 cage

	* Changelog,
	* cl-colors2.asd,
	* colors.lisp,
	* introduction.org,
	* test.lisp:

	- fixed CSS RGB alpha value, the range is [0-1].
	- allowed value none when parsing alpha component of CSS RGB color
	definition.
	- incresed version number;
	- updated documentation;
	- updated documentation.

2024-03-23 cage

	* colors.lisp,
	* print.lisp,
	* test.lisp:

	- added '/' when printing alpha component of CSS RGB.
	- added percent weights in parsing of CSS RGB.

2024-03-21 cage

	* cl-colors2.asd,
	* colors.lisp,
	* package-template.lisp,
	* package.lisp,
	* test.lisp:

	- improved parsing of CSS HSV and HSL to parse also non integer
	numbers;
	- added HSV, HSL parsing tests.
	- rewritten CSS RGB parser;
	- deprecated 'rgb/a-to-rgb'.
	- added parsing CSS RGB color definitions test.
	- fixed parsing of CSS colors, allowed spaces after open parenthesis.
	- fixed parsing of alpha component of CSS RGB color definition.

2024-03-20 cage

	* colors.lisp,
	* package-template.lisp,
	* package.lisp,
	* print.lisp,
	* test.lisp:

	- added functions to parse strings like: 'hsv(0, 50%, 50%)' to rgb
	struct.
	- fixed regex for parsing hsl and hsv CSS strings;
	- exported hsl related symbols;
	- changed format of CSS color formatting (removed commas and value
	rounding);
	- added or changed tests.

2023-06-20 cage

	* README.org,
	* cl-colors2.asd:

	- fixed references to old repository.

2023-06-16 cage

	* cl-colors2.asd:

	- increased version number.

2023-06-14 Artyom Bologov, cage

	* cl-colors2.asd,
	* colors.lisp,
	* hexcolors.lisp,
	* package-template.lisp,
	* package.lisp,
	* print.lisp,
	* test.lisp:

	colors(as-rgb): Improve hex parsing.
	print: Add RGB/HSV printing functions.
	Merge branch 'improve-hex-as-rgb' of aartaka/cl-colors2 into master
	Merge branch 'print-colors' of aartaka/cl-colors2 into master
	- added new contributor's name;
	- renamed print functions that output CSS color names.
	- added more tests;
	- added struct for HSV colorspace.
	- added HSL color space.

2023-05-11 cage

	* Changelog,
	* cl-colors2.asd,
	* colors.lisp:

	- removed warning.
	- updated changelog.

2023-05-10 cage

	* Changelog,
	* cl-colors2.asd,
	* colors.lisp,
	* introduction.org,
	* introduction.txt,
	* package.lisp,
	* test.lisp:

	- fixed 'as-hsv' compiler macro amd added the new function 'as-hsv'
	that is going to replace 'as-hsv'.
	- increased version number.
	- updated changelog.

2023-05-09 cage

	* cl-colors2.asd,
	* colors.lisp,
	* introduction.org,
	* introduction.txt,
	* test.lisp:

	- increased version number;
	- fixed 'as-rgb' compiler macro;
	- fixed the tests so that they compile and run flawlessly.
	- added 'errorp' keyword argument to 'as-rgb' function;
	- updated documentation.

2023-03-03 cage

	* Changelog,
	* cl-colors2.asd,
	* colors.lisp:

	- parsed color names containing spaces.
	- increased version number;
	- updated changelog.

2023-02-26 cage

	* Changelog,
	* cl-colors2.asd,
	* colors.lisp,
	* introduction.org,
	* introduction.txt:

	- specialized 'as-rgb' and 'as-hsv' to integer (convert integer to
	color struct).
	- updated documentation;
	- ensured float components in 'as-rgb' speacialized on integer.
	- increased version number.

2022-12-05 Artyom Bologov, cage

	* Changelog,
	* colornames-gdk.lisp:

	colornames-gdk: Add the non-hyphenated color names.
	Merge branch 'more-gdk-colors' of aartaka/cl-colors2 into master
	- updated changelog.

2022-12-04 cage

	* cl-colors2.asd,
	* colors.lisp:

	- moved  call  to  %lookup-colors-list' so  that  '(as-rgb
	color-name)
	returns an RGB struct not a symbol
	In a  previous commit the colors  list's structure was modified  and a
	lookup returned the symbol corresponding to the color:
	(as-rgb "blue") ; → +BLUE+
	this patch fixed that behaviour returning an RGB struct:
	(as-rgb "blue") ; → #S(CL-COLORS2:RGB :RED 0 :GREEN 0 :BLUE 1)
	- increased version number.

2022-12-03 cage

	* Changelog,
	* colors.lisp,
	* introduction.org,
	* introduction.txt,
	* package.lisp,
	* test.lisp:

	- added newlines to separate toplevel forms.
	- added key parameter for lookup table in 'as-rgb';
	- added offending color to error  message generated when parsing fails
	in: 'as-rgb'.
	- prevented duplicated lookup in 'as-rgb';
	- added a couple of tests about color name lookup.
	- updated docstrings.
	- updated documentation
	- fixed 'as-hsv' by specialyzing the method on symbol.
	- updated Changelog.

2022-12-02 cage

	* README.org,
	* colors.lisp,
	* introduction.org,
	* introduction.txt,
	* package.lisp:

	- updated documentation.
	- added a generic special variable: '*colors-list*' for color lookup.
	- moved macro definition to avoid a compiler warning.

2022-12-01 cage

	* colors.lisp:

	Merge branch 'rgba-and-hex-parsing' of aartaka/cl-colors2 into master
	- removed warning about functions called before macros, with the same
	name, were defined.

2022-11-30 cage, Artyom Bologov

	* cl-colors2.asd,
	* colornames-gdk.lisp,
	* colors.lisp,
	* package.lisp:

	Add GDK-parsed colors.
	Merge branch 'gdk-colors' of aartaka/cl-colors2 into master
	colors: Update color string parsing.

2021-10-06 cage

	* Changelog,
	* colors.lisp:

	- fixed compiler macro: 'as-hsv'.
	- added Changelog.

2021-08-28 cage

	* README.org:

	- updated README.

2021-08-21 SATO Shinichi, cage

	* cl-colors2.asd,
	* colornames-svg.lisp,
	* test.lisp:

	Fixed svg colors denominator, use 255 rather than 256.
	Merge branch 'fix-svg-color-denominator' of hyotang666/cl-colors2 into
	master
	- increade version number;
	- fixed tests (clunit2 compares multiple values)..

2021-03-02 cage

	* cl-colors2.asd,
	* colornames-x11.lisp,
	* package-template.lisp,
	* parse-x11-colors.lisp:

	Merge branch 'master' of guicho2.71828/cl-colors2 into master
	- updated 'package-template.lisp'.
	- fixed   "parse-x11-colors.lisp",   it    uses   the   new   filename
	("colornames-x11.lisp")  and the  new  "compressed"  form for  color
	definition.
	- updated version number and removed contact email.

2021-03-01 Masataro Asai

	* .gitignore,
	* README.org,
	* cl-colors2.asd,
	* colornames-svg.lisp,
	* colornames.lisp,
	* colornames.lisp => colornames-x11.lisp,
	* colors.lisp,
	* package.lisp,
	* test.lisp:

	gitignore
	compress X11 color file
	renamed: colornames -> colornames-x11
	svg color names
	README: reference links
	direct conversion from HEX string into HSV.
	more documentations
	compiler macro for as-hsv
	loading the test system runs the test automatically. test-system on
	cl-colors2 works
	README: testing instructions

2019-12-31 cage

	* colornames.lisp,
	* colors.lisp,
	* hexcolors.lisp,
	* package-template.lisp,
	* package.lisp,
	* parse-x11-colors.lisp,
	* test.lisp:

	- [BREAKS API] changed package name from cl-color to cl-colors2
	The former name broke quicklisp :(

2019-12-13 cage

	* colors.lisp,
	* package.lisp,
	* test.lisp:

	- added function 'color-equals'.

2018-09-12 cage

	* colors.lisp:

	- using compiler-macro for 'as-rgb'.

2018-09-04 cage

	* Makefile,
	* cl-colors2.asd,
	* colornames.lisp,
	* colors.lisp,
	* package.lisp,
	* parse-x11-colors.lisp:

	- added parsing of color names having spaces;
	- using actual color in *x11-colors-list* not a symbol.

2018-08-26 cage

	* README.org,
	* cl-colors2.asd,
	* colornames.lisp,
	* colors.lisp,
	* package-template.lisp,
	* package.lisp,
	* parse-x11-colors.lisp,
	* test.lisp:

	- fixed URL of the issue tracker in README.
	- exported copy-* symbols, removed non-existents ones.
	- "as-rgb" accept a color name as  parameter and makes a lookup on the
	list of parsed X11 colors.

2018-07-18 cage

	* cl-colors2.asd:

	- updated email.

2018-04-15 cage

	* README.org,
	* cl-colors.asd => cl-colors2.asd,
	* cl-colors2.asd:

	- changed name of the systems.
	- updated README to mark this version as a fork;
	- updated asdf's metadata.

2018-03-08 cage

	* COPYING,
	* README.org,
	* cl-colors.asd,
	* colors.lisp,
	* hexcolors.lisp,
	* package.lisp,
	* test.lisp:

	- removed orphaned notice;
	- removed dependency on 'let-plus'.
	- added license file.
	- using 'clunit2' instead of 'lift' as unit test library.
	- changed link for reporting issues plus others minor modifications to
	README.

2018-03-07 Tamas K. Papp

	* README.org:

	Added note about unsupported.
	Merge branch 'master' of github.com:tpapp/cl-colors
	Fixed formatting.

2015-12-10 Tamas K. Papp


	Merge pull request #10 from rick-monster/master

2015-12-07 The Hairy One

	* colors.lisp:

	this is a fix to make cl-cairo2 compile on ccl

2015-01-29 tpapp, Olof-Joachim Frahm

	* package.lisp,
	* parse-x11-colors.lisp:

	Merge branch 'development'
	Fix exported symbol names.
	Merge pull request #9 from Ferada/master

2015-01-26 tpapp

	* colors.lisp,
	* package-template.lisp,
	* package.lisp,
	* parse-x11-colors.lisp:

	Rewrote parsing of color definitions.
	Now explicitly generating the explort list, instead of using the color
	definition macro. Requires no manual manipulation of export list.

2014-06-26 tpapp


	Merge pull request #5 from rpav/master

2014-06-25 Ryan Pavlik

	* colors.lisp:

	Use DEFPARAMTER instead of DEFINE-CONSTANT

2014-02-16 Olof-Joachim Frahm, tpapp

	* cl-colors.asd,
	* hex-colors.lisp,
	* hex-colors.lisp => hexcolors.lisp,
	* package.lisp,
	* test.lisp:

	Added and exported PRINT- and PARSE-HEX-RGB.
	Add some test cases.
	Merge pull request #4 from Ferada/hex-colors
	Minor corrections.
	1. renamed hex-colors.lisp to hexcolors.lisp, consistent with the
	system
	definition
	2. unknown :stream keyword replaced by :destination.

2012-06-28 tpapp

	* Makefile,
	* README,
	* README.org,
	* cl-colors.asd,
	* colornames.lisp,
	* colors.lisp,
	* package.lisp,
	* parse-x11-colors.lisp,
	* parse-x11.lisp,
	* test.lisp:

	Added RGB, removed whitespace.
	Facelift and minor fixes.
	- README rewritten with an example, in org-mode
	- license changed to BOOST, version bumped to 0.2
	- new dependencies, in particular let-plus, appropriate structure
	accessor forms provided
	- colors are now structures, not classes
	- removed alpha channel stuff, does not belong here
	- autogenerated color names now have a small docstring
	- code prettified a bit, minor renamings
	- some files renamed
	Integrated the tests (actually, a single one) into a formal framework.

2010-03-30 tpapp

	* colors.lisp,
	* package.lisp:

	Addex hex->rgb.

2010-01-28 tpapp

	* colors.lisp:

	Made type declaration conforming to CLHS.

2009-05-21 tpapp

	* Makefile,
	* README,
	* cl-colors.asd,
	* colornames.lisp,
	* colors.lisp,
	* introduction.txt,
	* package.lisp,
	* parse-x11.lisp,
	* test.lisp:

	first commit
	License change.
