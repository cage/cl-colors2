* cl-colors: a simple color library for Common Lisp

* The development of this library has been stopped on this repository, please consider [[https://codeberg.org/cage/cl-colors-ng][cl-colors-ng]], instead.


This is a very simple color library for Common Lisp, providing:

1. Types for representing colors in HSV and RGB spaces.
2. Simple conversion  functions  between the  above  types (and  also
   hexadecimal representation for RGB).
3. Some predefined colors as an  association list of names and symbols
   which are bound to a corresponding RGB value.
   1. [[https://en.wikipedia.org/wiki/X11_color_names][X11 color names]] as
      ~*x11-color-list*~
   2. [[https://www.w3.org/TR/2018/REC-css-color-3-20180619/#html4][SVG basic color names]]
       as ~*svg-colors-list*~
   3. [[https://www.w3.org/TR/2018/REC-css-color-3-20180619/#svg-color][SVG extended color names]] as =*svg-extended-colors-list*=
   4. Example:

      #+BEGIN_SRC lisp

        '(("lightgreen"  . +LIGHTGREEN+)
          ("light-green" . +LIGHT-GREEN+)
          ("darkred"     . +DARKRED+)
         …

      #+END_SRC
** Examples

#+BEGIN_SRC lisp
(let ((color1 (hsv 107 62/100 52/100))  ; greenish
      (color2 (rgb 14/15 26/51 14/15))  ; = violet from X11
      (color3 (as-rgb "ff9e00")))       ; from hexadecimal
  (list                                 ;
   (as-rgb color1)                      ; converting to RGB
   (rgb-combination color1 +blue+ 0.4)  ; HSV autoconverted to RGB
   (hsv-combination color2 +blue+ 0.4)  ; RGB autoconverted to HSV
   color3))
#+END_SRC

evaluates to

#+BEGIN_EXAMPLE
'(#S(RGB :RED 20059/75000 :GREEN 13/25 :BLUE 247/1250)
  #S(RGB :RED 0.160472 :GREEN 0.312 :BLUE 0.51856) ; observe float contagion
  #S(HSV :HUE 60.0 :SATURATION 0.6722689 :VALUE 0.96000004)
  #S(RGB :RED 1 :GREEN 158/255 :BLUE 0))
#+END_EXAMPLE

Observe the  float contagion:  ~cl-colors~ functions don't  care about
the type of  the numbers as long  as they are a subtype  of ~real~ and
within the right range.

** Documentation

This library is so simple that it does not need a lot of documentation; please check the file [[./introduction.org][introduction.org]] or just look at the docstring in ~colors.lisp~.

** Testing

Run ~(ql:quickload :cl-colors2/tests)~.

** Regeneration of the X11 color names

Normally you should  not need to do this, the  sources already contain
the autogenerated file ~colornames.lisp~.  However, if for some reason
you need  to regenerate  this, you  can use  ~make~.  Even  though the
library itself  does not  depend on X11,  regenerating this  file will
require the appropriate file in X11.

** Bugs and issues

Please report them on [[https://codeberg.org/cage/cl-colors2/issues][Notabug]].

* Notes

This  is a  fork of  https://github.com/tpapp/cl-colors, the  original
author marked it as unsupported.
